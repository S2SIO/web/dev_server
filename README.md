# Environnement de développement Web en Python

Il est très facile de mettre en place un serveur de développement en Python.
Nous allons d’abord commencer par récupérer le code d’un projet dédié sur la forge logicielle Framagit.
À l’aide d’un terminal virtuel, nous exécutons la commande suivante :
```
git clone git@framagit.org:S2SIO/web/dev_server.git
```

## Propulser du code HTML
Nous nous plaçons alors dans le dossier `html` du projet :
```
cd dev_server/html
```

La seule exécution de la commande suivante suffit à propulser un serveur
de développement Python, qui écoute sur le port `8000` :
```
python3 -m http.server
```

Nous chargeons alors l’adresse `localhost:8000` dans le navigateur Firefox.
Par défaut, le serveur liste les fichiers présents dans le répertoire courant.
Nous sélectionnons alors le fichier `formulaire.html`.

Si nous soumettons le formulaire, la connexion échoue, car nous n’avons
pas encore lancé notre serveur de développement écoutant sur le port `8888`.
La console du terminal détaille le code erreur envoyé :
```
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
127.0.0.1 - - [05/Mar/2021 17:17:43] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [05/Mar/2021 17:17:45] code 404, message File not found
127.0.0.1 - - [05/Mar/2021 17:17:45] "GET /favicon.ico HTTP/1.1" 404 -
127.0.0.1 - - [05/Mar/2021 17:20:35] "GET /formulaire.html HTTP/1.1" 200 -
```

Nous arrêtons le serveur à l’aide de la combinaison de touches « Ctrl + C »,
puis nous exécutons le script `server.py` qui produit le même effet :
```
./server.py
```

## Propulser du code Python
Nous ouvrons alors un nouvel onglet dans le terminal virtuel
(à l’aide d’un menu Fichier ou du raccourci clavier « Ctrl + Maj + T »).
Nous plaçons maintenant dans le dossier `cgi` de notre projet :
```
cd ../cgi
```

Et nous exécutons le script `server.py` de la façon suivante :
```
./server.py
```

Le résultat suivant s’affiche alors en console :
```
Serveur actif sur le port : 8888
```

Notre second serveur écoute sur le port `8888`.
Si nous ouvrons l’adresse `localhost:8888` dans le navigateur Firefox, nous obtenons une erreur.
Ce serveur ne peut gérer que les scripts python CGI. En effet, si nous observons le code,
nous constatons que la variable `handler` ne traite que les requêtes CGI.

Par contre notre serveur, peut désormais propulser tous les scripts python
CGI placés dans le même dossier et donc notamment le script `traite_form_3_1.py`,
par ailleurs déjà configuré avec des droits d’exécution.

Si nous soumettons de nouveau le formulaire précédent, les données sont alors traitées,
comme en témoigne d’ailleurs le résultat dans la console de notre serveur CGI :
```
127.0.0.1 - - [05/Mar/2021 17:40:26] "POST /traite_form_3_1.py HTTP/1.1" 200 -
```

Le script `traite_form_3_1.py` est rigoureusement identique à la [version] utilisée dans le TP précédent.
Nous constatons alors que notre environnement de développement est prêt !

[version]: https://framagit.org/S2SIO/web/cgi/-/blob/master/cgi-bin/traite_form_3_1.py
